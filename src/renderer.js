const $ = window.$ = window.jQuery = require('jquery');
window.Tether = require('tether');
window.Bootstrap = require('bootstrap');
const path = require('path');
const fs = require('fs');
const {
  exec,
  execSync
} = require('child_process');

const buildCLI = require(`${__dirname}\\cli.js`);
let config = require(`${__dirname}\\servers.json`);

function serverID(serverName) {
  return serverName.replace(/[\s.,\/#!$%\^&\*;:{}=\-_`~()]/g, '_');
}

function reloadConfig() {
  delete require.cache[require.resolve(`${__dirname}\\servers.json`)];
  config = require(`${__dirname}\\servers.json`);
}

function setFirstItemActive() {
  const firstServer = $('.server-list').text().trim().split('\n')[0];
  $(`.${serverID(firstServer)}`).addClass('active');
  return firstServer;
}

function loadServerConfig(worldName) {
  const worldObj = config[worldName];

  if (worldObj) {
    $('#serverTitle').text(worldName);
    $('#buttonSelect').text(worldName);
    // $('#edit').text(`Edit ${worldName}`);
    // $('#delete').text(`Delete ${worldName}`);
    $('#port').text(worldObj.port);
    $('#portForward').text(worldObj.portForward === 'y' ? 'Yes' : 'No');
    $('#players').text(worldObj.maxPlayers);

    $('#worldNameEdit').val(worldObj.worldFile);
    $('#modalLabelEdit').text(`Edit ${worldName}`);
    $('#portEdit').val(worldObj.port);
    $('#portForwardEdit').prop('checked', worldObj.portForward === 'y');
    $('#passwordEdit').val(worldObj.serverPassword);
    $('#maxPlayersEdit').val(worldObj.maxPlayers);

    hidePassword();
  } else {
    $('#serverTitle').text('');
    $('#buttonSelect').text('');
    // $('#edit').text('Edit');
    // $('#delete').text('Delete');
    $('#port').text('');
    $('#portForward').text('');
    $('#password').text('');
    $('#players').text('');

    $('#worldNameEdit').val('');
    $('#modalLabelEdit').text(`Edit`);
    $('#portEdit').val('');
    $('#portForwardEdit').prop('checked', true);
    $('#passwordEdit').val('');
    $('#maxPlayersEdit').val('');
  }
}

function initServerList(activeElement) {
  const servers = Object.keys(config);

  servers.forEach((server, ind) => {
    const serverListItem =
      `<li class="nav-item">
         <a href="#${serverID(server)}" class="${serverID(server)} server-item nav-link${server === activeElement ? ' active': ''}">${server}</a>
       </li>`;

    $('.server-list').append(serverListItem);
  });

  sortServerList('servers');
  sortServerList('serversSmall');
}

function startServer(worldName) {
  buildCLI(['node', 'cli.js', 'start', worldName]).parse();
}

function deleteServer(worldName) {
  $('.server-list').empty();
  buildCLI(['node', 'cli.js', 'delete', worldName]).parse();
  reloadConfig();
  initServerList();
  const firstServer = setFirstItemActive();
  loadServerConfig(firstServer);

  setItemClick();
}

function save(worldName) {
  const newWorldFile = $('#worldNameEdit').val();
  const players = $('#maxPlayersEdit').val();
  const port = $('#portEdit').val();
  const password = $('#passwordEdit').val() || '';
  const portForward = $('#portForwardEdit').prop('checked') ? 'y' : 'n';

  const newWorldName = path.basename(newWorldFile, '.wld').replace(/_/g, ' ');

  if (newWorldName !== worldName) {
    buildCLI(['node', 'cli.js', 'delete', worldName]).parse();
    buildCLI(['node', 'cli.js', 'add', newWorldFile, '-m', players, '-p', port, '-f', portForward, '-s', password]).parse();
  } else {
    buildCLI(['node', 'cli.js', 'edit', newWorldName, '-m', players, '-p', port, '-f', portForward, '-s', password]).parse();
  }

  reloadConfig();
  $('.server-list').empty();
  $('.active').removeClass('active');
  initServerList(newWorldName);
  loadServerConfig(newWorldName);

  setItemClick();
}

function add() {
  const newWorldName = $('#worldNameAdd').val();
  const isTModLoader = $('#tModLoader').prop('checked');
  const size = $('#size').val();
  const difficulty = $('#difficulty').val();
  const players = $('#maxPlayersAdd').val();
  const port = $('#portAdd').val();
  const password = $('#passwordAdd').val();
  const portForward = $('#portForwardAdd').prop('checked') ? 'y' : 'n';

  const args = ['node', 'cli.js', 'create', newWorldName, '-s', size, '-d', difficulty, '-m', players, '-p', port, '-f', portForward, '-S', password];

  if (isTModLoader) {
    args.push('-t');
  }

  buildCLI(args).parse();

  $('#worldNameAdd').val('');
  $('#size').val('1');
  $('#difficulty').val('0');
  $('#tModLoader').prop('checked', false);
  $('#portAdd').val('');
  $('#portForwardAdd').prop('checked', true);
  $('#passwordAdd').val('');
  $('#maxPlayersAdd').val('');

  reloadConfig();
  $('.server-list').empty();

  initServerList(newWorldName);
  loadServerConfig(newWorldName);
  $(`.${serverID(newWorldName)}`).addClass('creating');
  // $(`.${serverID(newWorldName)}`).addClass('fa');
  // $(`.${serverID(newWorldName)}`).addClass('fa-spinner');

  const creating = setInterval(() => {
    if (fs.existsSync(config[newWorldName].worldFile)) {
      $(`.${serverID(newWorldName)}`).removeClass('creating');
    }
  });

  setItemClick();
}

function sortServerList(id) {
  let list, i, switching, b, shouldSwitch;
  list = document.getElementById(id);
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    // Loop through all list items:
    for (i = 0; i < (b.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Check if the next item should
      switch place with the current item: */
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        /* If next item is alphabetically lower than current item,
        mark as a switch and break the loop: */
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark the switch as done: */
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
}

function hidePassword() {
  const worldName = $('#serverTitle').text();
  const password = config[worldName].serverPassword;
  const hiddenPassword = password.replace(/./g, '*');

  $('#password').text(hiddenPassword);

  const eyeIcon = $('#eye');
  eyeIcon.removeClass('fa-eye-slash');
  eyeIcon.addClass('fa-eye');
}

initServerList();

function setItemClick() {
  $(".server-item").click((el) => {
    $('.active').removeClass('active');
    loadServerConfig($(el.currentTarget).text());
    const serverClass = $(el.currentTarget).attr('class').toString().split(' ').find(c => !['server-item', 'nav-link', 'active'].includes(c));
    $(`.${serverClass}`).addClass('active');
  });
}

setItemClick();

$('#start').click((el) => {
  startServer($('#serverTitle').text());
});

$('#delete').click((el) => {
  $('#deleteTitle').text(`Delete ${$('#serverTitle').text()}`)
});

$('#deleteConfirm').click((el) => {
  deleteServer($('#serverTitle').text());
});

$('#saveEdits').click((el) => {
  save($('#serverTitle').text());
});

$('#saveNew').click((el) => {
  add();
});

$('#revealPassEdit').mousedown(() => {
  $('#passwordEdit').clone().attr('type', 'text').insertAfter('#passwordEdit').prev().remove();
});

$('#revealPassEdit').mouseup(() => {
  $('#passwordEdit').clone().attr('type', 'password').insertAfter('#passwordEdit').prev().remove();
});

$('#revealPassDashboard').click(() => {
  const eyeIcon = $('#eye');

  if (eyeIcon.hasClass('fa-eye')) {
    const worldName = $('#serverTitle').text();
    const password = config[worldName].serverPassword;

    $('#password').text(password);
    eyeIcon.removeClass('fa-eye');
    eyeIcon.addClass('fa-eye-slash');
  } else {
    hidePassword();
  }
});

if (Object.keys(config).length > 0) {
  const firstServer = setFirstItemActive();
  loadServerConfig(firstServer);
}

setInterval(() => {
  const selected = $('.active');
  const start = $('#start');
  const deleteButton = $('#delete');
  const title = $('#serverTitle');

  const isCreating = selected.hasClass('creating');

  start.prop('disabled', isCreating);
  deleteButton.prop('disabled', isCreating);

  if (isCreating) {
    if (!title.hasClass('creating')) {
      title.addClass('creating');
      const text = selected.text().substring(0, selected.text().length / 2);
      title.html(`${text} <i class="fas fa-spinner fa-spin"></i>`)
    }
  } else {
    if (title.hasClass('creating')) {
      const text = selected.text().substring(0, selected.text().length / 2);
      title.removeClass('creating');
      title.html(text)
    }
  }
}, 10);
