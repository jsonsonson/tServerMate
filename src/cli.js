#!/usr/bin/env node

const cli = require('wily-cli');
const fs = require('fs');
const path = require('path');
const {
  execSync,
  spawn,
  exec,
} = require('child_process');
const homedir = require('homedir');
let config;

let serversFile = `${__dirname}\\servers.json`;
const tModLoaderWorldsDir = `${homedir()}\\Documents\\My Games\\Terraria\\ModLoader\\Worlds`;
const worldsDir = `${homedir()}\\Documents\\My Games\\Terraria\\Worlds`;
const configFile = `${__dirname}\\serverconfig.txt`;
const settingsFile = `${__dirname}\\settings.txt`;
let terrariaDir;

function getServerEXEFile(isTModLoader) {
  return isTModLoader ? 'tModLoaderServer.exe' : 'TerrariaServer.exe';
}

function getEXEPath(exeFile) {
  return `${terrariaDir}\\${exeFile}`;
}

function loadWorlds(dir) {
  const items = fs.readdirSync(dir);

  if (items.length > 0) {
    items.forEach(item => {
      if (item.split('.').pop() === 'wld') {
        const worldName = item.split('.')[0].replace(/_/g, ' ');

        if (!config[worldName]) {
          config[worldName] = {
            maxPlayers: "8",
            port: 7777,
            portForward: "y",
            serverPassword: "",
            worldFile: `${dir}\\${item}`,
            serverEXE: getServerEXEFile(dir.toLowerCase().includes('modloader')),
          };
        }
      }
    });
  }

  writeConfig();
}

function initSettings() {
  try {
    let contents;

    if (!fs.existsSync(settingsFile)) {
      fs.writeFileSync(settingsFile, `terraria_directory=C:\\Program Files (x86)\\Steam\\steamapps\\common\\Terraria\r\n`);
      contents = fs.readFileSync(settingsFile).toString().split('\r\n');
    } else {
      contents = fs.readFileSync(settingsFile).toString().split('\r\n');

      if (!contents.find(setting => setting.includes('terraria_directory'))) {
        contents.push(`terraria_directory=C:\\Program Files (x86)\\Steam\\steamapps\\common\\Terraria`);
        fs.writeFileSync(settingsFile, contents.join('\r\n'));
      }
    }

    terrariaDir = contents.find(setting => setting.includes('terraria_directory')).split("=")[1].trim();
  } catch (err) {

  }
}

function initConfig() {
  try {
    if (!fs.existsSync(serversFile)) {
      fs.writeFileSync(serversFile, '{}');
    }

    if (config == undefined) {
      config = require(`${serversFile}`);
    }

    loadWorlds(worldsDir);
    loadWorlds(tModLoaderWorldsDir);

    Object.keys(config).forEach(server => {
      const worldFile = config[server].worldFile;

      if (!fs.existsSync(worldFile)) {
        delete config[server];
      }
    });

    writeConfig();
  } catch (err) {
    console.log(err);
    process.exit(-1);
  }
}

function writeConfig() {
  const configStr = JSON.stringify(config, null, '  ');
  fs.writeFileSync(serversFile, configStr);
}

function addServer(worldName, options, parameters) {
  if (worldName !== undefined && `${worldName}`.length > 0) {
    if (!Object.keys(config).includes(worldName)) {
      const worldObj = {
        maxPlayers: '8',
        port: '7777',
        portForward: 'y',
        serverPassword: '',
        worldFile: parameters.worldFile,
        serverEXE: getServerEXEFile(parameters.worldFile.toLowerCase().includes('modloader'))
      };

      try {
        if (options.maxPlayers !== undefined && Number(options.maxPlayers) > 0) {
          worldObj.maxPlayers = `${options.maxPlayers}`;
        }
      } catch (err) {
        // do nothing
      }

      try {
        if (options.port !== undefined && Number(options.port) > 0) {
          worldObj.port = options.port;
        }
      } catch (err) {
        // do nothing
      }

      if (options.portForward !== undefined && `${options.portForward}`.length > 0 && ['y', 'n'].includes(options.portForward.trim().toLowerCase())) {
        worldObj.portForward = `${options.portForward}`.trim().toLowerCase();
      }

      if (options.serverPassword !== undefined && `${options.serverPassword}`.length > 0) {
        worldObj.serverPassword = `${options.serverPassword}`;
      }

      config[worldName] = worldObj;
      writeConfig();
      console.log(`"${worldName}" added`);
    } else {
      console.log(`"${worldName}" already exists. Use the "edit" command if you would like to make any changes`);
    }
  }
}

function editServer(worldName, options) {
  if (worldName !== undefined && `${worldName}`.length > 0) {
    initConfig();

    if (Object.keys(config).includes(worldName)) {
      const worldObj = config[worldName];

      try {
        if (options.maxPlayers !== undefined && Number(options.maxPlayers) > 0) {
          worldObj.maxPlayers = `${options.maxPlayers}`;
        }
      } catch (err) {
        // do nothing
      }

      try {
        if (options.port !== undefined && Number(options.port) > 0) {
          worldObj.port = options.port;
        }
      } catch (err) {
        // do nothing
      }

      if (options.portForward !== undefined && `${options.portForward}`.length > 0 && ['y', 'n'].includes(options.portForward.trim().toLowerCase())) {
        worldObj.portForward = `${options.portForward}`.trim().toLowerCase();
      }

      if (options.serverPassword !== undefined) {
        worldObj.serverPassword = `${options.serverPassword}`;
      }

      config[worldName] = worldObj;
      writeConfig();
      console.log(`"${worldName}" updated`);
    } else {
      console.log(`"${worldName}" does not exist. Use the "add" command if you would like to add a new server configuration`);
    }
  }
}

function deleteServer(worldName) {
  if (worldName !== undefined && `${worldName}`.length > 0) {
    initConfig();

    if (Object.keys(config).includes(worldName)) {
      const dir = path.dirname(config[worldName].worldFile);
      const deleteFiles = [
        `${dir}\\${worldName.replace(/\s/g, '_')}.wld`,
        `${dir}\\${worldName.replace(/\s/g, '_')}.twld`,
        `${dir}\\${worldName.replace(/\s/g, '_')}.wld.bak`,
        `${dir}\\${worldName.replace(/\s/g, '_')}.twld.bak`,
      ];

      deleteFiles.forEach(file => {
        if (fs.existsSync(file)) {
          fs.unlinkSync(file);
        }
      });

      delete config[worldName];
      writeConfig();
      console.log(`"${worldName}" deleted`);
    } else {
      console.log(`"${worldName}" does not exist`);
    }
  }
}

function startServer(worldName, options) {
  if (worldName !== undefined && `${worldName}`.length > 0) {
    initConfig();

    if (Object.keys(config).includes(worldName)) {
      console.log(`Starting "${worldName}"...`);

      const worldObj = config[worldName];
      const worldFilepath = worldObj.worldFile;

      const options = ['-secure'];

      if (worldObj.maxPlayers) {
        options.push(`-players ${worldObj.maxPlayers}`);
      } else {
        options.push(`-players 8`);
      }

      if (worldObj.port) {
        options.push(`-port ${worldObj.port}`);
      } else {
        options.push(`-port 7777`);
      }

      if (worldObj.serverPassword) {
        options.push(`-pass ${worldObj.serverPassword}`);
      }

      if (worldObj.portForward && worldObj.portForward === 'n') {
        options.push('-noupnp');
      }

      const terrariaServerFile = getEXEPath(worldObj.serverEXE);
      console.log(path.resolve(terrariaServerFile));

      try {
        if (process.env.ELECTRON) {
          const serverProcess = exec(`start cmd.exe /c "${terrariaServerFile}" -world ${worldFilepath} ${options.join(' ')}`);
        } else {
          const serverProcess = execSync(`"${terrariaServerFile}" -world ${worldFilepath} ${options.join(' ')}`, {
            stdio: 'inherit'
          });
          serverProcess.stdout.pipe(process.stdout);
        }
      } catch (err) {

      }
    } else {
      console.log(`"${worldName}" does not exist. Use the "add" command if you would like to add a new server configuration`);
    }
  }
}

function createServer(worldName, options) {
  if (worldName !== undefined && `${worldName}`.length > 0) {
    if (!Object.keys(config).includes(worldName)) {
      const worldFile = (options.tModLoader ? tModLoaderWorldsDir : worldsDir) + `\\${worldName.replace(/\s/g, '_')}.wld`;
      const terrariaServerFile = getEXEPath(getServerEXEFile(options.tModLoader));

      addServer(worldName, options, { worldFile });
      writeTempConfig(worldName, worldFile, options);

      const serverProcess = exec(`"${terrariaServerFile}" -config ${configFile}`);

      try {
        let killing = false;
        serverProcess.stdout.on('data', (data) => {
          const text = data.toString();

          if (!killing && text.trim().toLowerCase().includes('choose world')) {
            killing = true;
            serverProcess.stdin.end();
          }
        });
      } catch (err) {
        console.log(err);
      }

      const interval = setInterval(() => {
        if (fs.existsSync(worldFile)) {
          console.log(`"${worldName}" created`);
          fs.unlinkSync(configFile);
          clearInterval(interval);
        }
      }, 10);
    } else {
      console.log(`"${worldName}" already exists. Use the "edit" command if you would like to make any changes`);
    }
  }
}

function writeTempConfig(worldName, worldFile, options) {
  let content =
  `world=${worldFile}\r\nautocreate=${options.size || 2}\r\nsecure=1\r\npriority=1\r\nmaxplayers=${options.maxPlayers || 8}\r\nport=${options.port || 7777}\r\npassword=${options.serverPassword || ''}\r\ndifficulty=${options.difficulty !== undefined || 0}\r\nworldname=${worldName}\r\n${options.portForward ? 'upnp=1' : ''}`;

  fs.writeFileSync(configFile, content);
}

function buildCLI(argv) {
  process.argv = argv;

  const startServerCLI = cli
    .parameter('worldName', true, false)
    .on('exec', (options, parameters) => {
      startServer(parameters.worldName);
    });

  const addServerCLI = cli
    .parameter('worldFile', true, false)
    .option('maxPlayers', 'Max player count', {
      default: 8,
    })
    .option('port', 'Server port', {
      default: 7777,
    })
    .option('portForward', 'Toggle automatic port forwarding on', {
      short: 'f',
    })
    .option('serverPassword', 'Server password')
    .on('exec', (options, parameters) => {
      const worldName = path.basename(parameters.worldFile, '.wld').replace(/_/g, ' ');
      addServer(worldName, options, parameters);
    });

  const editServerCLI = cli
    .parameter('worldName', true, false)
    .option('maxPlayers', 'Max player count', {
      default: 8,
    })
    .option('port', 'Server port', {
      default: 7777,
    })
    .option('portForward', 'Toggle automatic port forwarding on', {
      short: 'f',
    })
    .option('serverPassword', 'Server password')
    .on('exec', (options, parameters) => {
      editServer(parameters.worldName, options);
    });

  const deleteServerCLI = cli
    .parameter('worldName', true, false)
    .on('exec', (options, parameters) => {
      deleteServer(parameters.worldName);
    });

  const createServerCLI = cli
    .parameter('worldName', true, false)
    .option('tModLoader', 'Denotes this server as a tModLoader server')
    .option('size', 'Set the world size (1, 2, or 3)', {
      default: 2,
    })
    .option('difficulty', 'Set the world difficutly (0 or 1)', {
      default: 0,
    })
    .option('maxPlayers', 'Max player count', {
      default: 8,
    })
    .option('port', 'Server port', {
      default: 7777,
    })
    .option('portForward', 'Toggle automatic port forwarding on', {
      short: 'f',
    })
    .option('serverPassword', 'Server password')
    .on('exec', (options, parameters) => {
      createServer(parameters.worldName, options);
    });

  return cli
    .command('start', 'Start a Terraria server', startServerCLI)
    .command('add', 'Add server configuration for an existing Terraria world', addServerCLI)
    .command('create', 'Create a new Terraria server', createServerCLI)
    .command('edit', 'Edit a Terraria server configuration', editServerCLI)
    .command('delete', 'Delete a Terraria server configuration', deleteServerCLI)
    .command('list', 'List added Terraria worlds', () => {
      initConfig();
      const list = Object.keys(config).join('\n');
      console.log(list);
    })
    .command('info <worldName>', 'Display Terraria server configuration', (options, parameters) => {
      initConfig();
      const worldName = parameters.worldName;

      if (config[worldName]) {
        let info = config[worldName];
        info = JSON.stringify(info);
        console.log(info.substring(1, info.length - 1).trim().split(',').join('\n').replace(/"/g, '').replace(/:/g, ': '));
      } else {
        console.log(`"${worldName}" does not exist. Use the "add" command if you would like to add a new server configuration`);
      }
    });
}

initSettings();
initConfig();
buildCLI(process.argv);

module.exports = buildCLI;
